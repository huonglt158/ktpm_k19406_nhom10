const { I } = inject();
const timeout = require("../common/timeout");
const createNewApiKeyLocator = require("./locator");
const settingPageLocator = require("../settingPage/locator");
const MyVariable = require("../common/variable");
const customMethod = require("../../utils/customMethod");

module.exports = {
	createNewApiKey(apiKeyName, editApiKeyName) {
		I.waitForElement(createNewApiKeyLocator.settingTab, timeout.waitForElement);
		customMethod.clickElement(createNewApiKeyLocator.settingTab);
		I.waitForElement(
			createNewApiKeyLocator.apiSettingTab,
			timeout.waitForElement
		);
		customMethod.clickElement(createNewApiKeyLocator.apiSettingTab);
		I.waitForElement(
			createNewApiKeyLocator.createApiButton,
			timeout.waitForElement
		);
		customMethod.clickElement(createNewApiKeyLocator.createApiButton);
		I.waitForElement(
			createNewApiKeyLocator.inputApiKeyName,
			timeout.waitForElement
		);
		customMethod.fieldValue(createNewApiKeyLocator.inputApiKeyName, apiKeyName);
		I.waitForElement(
			createNewApiKeyLocator.createButton,
			timeout.waitForElement
		);
		customMethod.clickElement(createNewApiKeyLocator.createButton);
		I.waitForElement(
			createNewApiKeyLocator.successText,
			timeout.waitForElement
		);
		customMethod.clickElement(createNewApiKeyLocator.exitCreateApi);
		I.waitForElement(
			createNewApiKeyLocator.editNameApiButton,
			timeout.waitForElement
		);
		customMethod.clickElement(createNewApiKeyLocator.editNameApiButton);
		I.waitForElement(
			createNewApiKeyLocator.inputApiKeyName,
			timeout.waitForElement
		);
		customMethod.fieldValue(
			createNewApiKeyLocator.inputApiKeyName,
			editApiKeyName
		);
		customMethod.clickElement(createNewApiKeyLocator.saveApiAfterEditButton);
		I.waitForElement(
			createNewApiKeyLocator.laterApiKeyName,
			timeout.waitForElement
		);
		I.seeTextEquals(editApiKeyName, createNewApiKeyLocator.laterApiKeyName);
		customMethod.clickElement(createNewApiKeyLocator.deleteApiKeyButton);
		I.waitForElement(
			createNewApiKeyLocator.acceptDeleteApiKey,
			timeout.waitForElement
		);
		customMethod.clickElement(createNewApiKeyLocator.acceptDeleteApiKey);
		I.waitForElement(
			createNewApiKeyLocator.verifyDeleteApiKey,
			timeout.waitForElement
		);
	},
};
