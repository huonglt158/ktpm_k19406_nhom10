module.exports = {
	settingButton: '//mat-icon[text()="settings"]',
	menuIcon:
		"//button[contains(@class,'mat-focus-indicator')]//span[@class='mat-button-wrapper']//mat-icon[text()='menu']",
	settingTab:
		"//span[contains(@class,'mat-content')]//p[contains(text(),'Thiết lập')]",
	apiSettingTab:
		"//a[contains(@class,'mat-list-item')]//span[contains(text(),'Api keys')]",
	createApiButton: '//div[@class="api-keys-header-action"]',
	createButton:
		'//button[@type="submit" and @class="mat-focus-indicator mat-raised-button mat-button-base"]',
	inputApiKeyName: '//input[@formcontrolname="name"]',
	successText: '//h4[contains(text(),"API Key đã được khởi tạo thành công")]',
	exitCreateApi: '//span[text()="Xong"]',
	editNameApiButton: '//mat-icon[text()="edit"]',
	saveApiAfterEditButton: '//span[text()=" Lưu thay đổi "]',
	laterApiKeyName: '//div[@class="custom-name"]//b',
	deleteApiKeyButton: '//mat-icon[text()="delete"]',
	acceptDeleteApiKey: '//span[contains(text(),"Đồng ý")]',
	verifyDeleteApiKey: '//div[contains(text(),"Bắt đầu khởi tạo API Key")]',
};
