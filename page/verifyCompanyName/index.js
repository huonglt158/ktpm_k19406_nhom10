const { I } = inject();
const verifyLocator = require("./locator");
const settingPageLocator = require("../settingPage/locator");
const timeout = require("../common/timeout");
const customMethod = require("../../utils/customMethod");

module.exports = {
	async verifyCompanyName(myCompanyName) {
		I.waitForElement(verifyLocator.menuIcon);
		customMethod.clickElement(verifyLocator.menuIcon);
		I.waitForElement(verifyLocator.profileNameLabel);
		I.seeTextEquals(myCompanyName, verifyLocator.profileNameLabel);
	},
};
