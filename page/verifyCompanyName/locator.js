module.exports = {
	// Bat dau bien
	companyNameDisplay:
		'//span[@class="mat-tooltip-trigger text-sm pl-3 ng-star-inserted"]',
	signalCompanyButton: '//div[@class="mat-list-avatar business-brand"]',
	toggleButton:
		'//mat-icon[@class="mat-icon notranslate material-icons mat-icon-no-color"]',
	profileNameLabel:
		"//a[contains(@class,'mat-list-item')]//div//span[contains(@class,'mat-tooltip-trigger text-sm pl-3 ng-star-inserted')]",
	menuIcon:
		"//button[contains(@class,'mat-focus-indicator')]//span[@class='mat-button-wrapper']//mat-icon[text()='menu']",
};
