const { I } = inject();
const createNewCompanyFunction = require("../page/createNewCompany/index.js");
const createApiKeyFunction = require("../page/createApiKey/index.js");
const createNewCompanyLocator = require("../page/createNewCompany/locator.js");
const Myfunctions = require("../page/common/functions");
const MyVariable = require("../page/common/variable.js");

Given("I create a new API key", () => {
	createApiKeyFunction.createNewApiKey(
		MyVariable.apiKeyName,
		MyVariable.editApiKeyName
	);
});
