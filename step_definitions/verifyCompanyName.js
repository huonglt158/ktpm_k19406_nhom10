const verifyCompanyNameFunction = require("../page/verifyCompanyName/index.js");
const MyVariable = require("../page/common/variable.js");

Given("I verify company name", () => {
	verifyCompanyNameFunction.verifyCompanyName(MyVariable.companyName);
});
